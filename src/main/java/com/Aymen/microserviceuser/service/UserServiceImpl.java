package com.Aymen.microserviceuser.service;

import com.Aymen.microserviceuser.model.User;
import com.Aymen.microserviceuser.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
   private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public User save(User user){
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userRepository.save(user);
    }

    @Override
    public User findByUserName(String userName){
        return userRepository.findByUserName(userName).orElse(null);
    }

    @Override
    public List<String> findUsers(List<Long> idList){
       return  userRepository.findByIdList(idList);
    }
}
