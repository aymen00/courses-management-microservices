package com.Aymen.microserviceuser.service;

import com.Aymen.microserviceuser.model.User;

import java.util.List;

public interface UserService {
    User save(User user);

    User findByUserName(String userName);

    List<String> findUsers(List<Long> idList);
}
